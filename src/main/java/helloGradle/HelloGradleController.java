package helloGradle;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController()
@RequestMapping("/api")
public class HelloGradleController {

    @GetMapping("/getMessage")

    public String helloGradle() {
        return "Hello Gradle!";
    }

}